			<?php
				require_once('../lib/contactolib.php');
				echo recaptcha_get_html($config["recaptcha_public_key"]);
			?>

			<input type="submit" value="enviar" name="submit" class="submit" />

			<div>
				<?php
					if (isset($status)) {
						if ($status) {
							echo 'Muchas gracias por contacterse con nosostros.';
						} else {
							echo 'Error en el formulario. Vuélvalo a intentar o intente ponerse en contacto por otro medio.';
						}
					}
				?>
			</div>
	
